# Flight Scheduler 
#@author Vinod Ramdin
#@version Python 2.7.0		
# Main
#Handles input and output from file
#   Initializes a Scheduler to maintain a queue of Airplane objects

from Scheduler import *

class Main(object):
    control = Scheduler()
    go = True
    timer = 0
    with open('new 2.txt') as f:
        while go:
            #if input('Continue?: ') != "no":
            print 'time: ' + str(timer)
            #requestPrompt = input('ID, Sub, Slot, Len: ')
            requestPrompt = f.readline()
            req_vars = requestPrompt.split(', ')
            id = req_vars[0]
            sub = req_vars[1]
            slot = req_vars[2]
            len = req_vars[3]
            control.add_req(id, sub, slot, len)
            control.print_queue()
            timer += 1
            #else:
            #    go = False
			
			

#scheduler
#Handles storage and updating of information about requests
#by maintaining and accessing a queue of Airplane objects

from Airplane import *

class Scheduler(object):

    def __init__(self):
        self.queue = []
        self.unqueue = []

    def add_req(self, id, sub, slot, len):            # add the new plane to the queue in the right order
        new_plane = Airplane(id, sub, slot, len)      # instantiate a new Airplane object with the variables from input
        new_index = 0
        queue_copy = list(self.queue)
        plane_added = False                           # plane not added
        for plane in queue_copy:                      # for each plane currently in the queue
            if plane.slot > new_plane.slot:           # check to see if the old plane's time slot is greater than the new plane's time slot
                i = self.queue.index(plane)           # get the index of the greater slotted plane
                self.queue.insert(i, new_plane)       # add the new plane to the queue before that one
                new_index = i
                plane_added = True                    # plane added
        if plane_added is False:                      # if plane still not added after whole list (if no plane requested a greater time slot)
            print 'added to end'
            self.queue.append(new_plane)              # add the new plane object to the queue at the end
            new_index = self.queue.index(new_plane)
        queue_copy = list(self.queue)
        for plane in queue_copy:
            new_index = self.queue.index(plane)
            self.get_start(self.queue[new_index])
        # self.get_start(self.queue[new_index])
        # print 'Start: ' + str(self.queue[new_index].start)

    def print_queue(self):
        for plane in self.queue:
            print plane.id + ' (Scheduled for: ' + str(plane.start) + '), '

    def get_start(self, plane):
        index = self.queue.index(plane)
        if index == 0:
            start = 0
        else:
            previous = self.queue[index-1]
            start = int(previous.start) + int(previous.len)
        plane.start = start
		
		
		

#Constructor 
# Stores the information for a single request

class Airplane(object):

    def __init__(self, new_id, new_sub, new_slot, new_len, new_start='', new_end=''):
        self.id = new_id  #all variables initilized
        self.sub = new_sub
        self.slot = new_slot
        self.len = new_len
        self.start = new_start
        self.end = new_end

